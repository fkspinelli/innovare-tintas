$(document).ready(function(){
	$('#banner').css({'height':$(window).height()});

	$('.row-equal').each(function(){

		if ($(window).width() > parseInt($(this).attr('data-max-width'))) {
			var max = 0;
			$(this).find('.col-height').each(function(){
				var height = $(this).height();
				if (height > max) {
					max = height;
				}
			});
			$(this).find('.col-height').css({'height':max});
		}
	});

	$('.links a, .navbar-brand').click(function(event){
		event.preventDefault();
		// if(!$('.navbar-toggle').hasClass('collapsed')){
		// 	$('.navbar-toggle').click();
		// }
		$('html, body').animate({
		  scrollTop: $($(this).attr('href')).offset().top 
		}, 1000);
	});
});