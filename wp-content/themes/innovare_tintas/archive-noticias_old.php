<?php
get_header();
$page_id = 42;
?>

    <div class="top-title-page">
      <div class="top-title text-center">
        <h1><?php echo get_the_title($page_id); ?></h1>
      </div>
    </div>

    <div id="bannerHome" class="carousel slide carousel-fade interna" data-ride="carousel">
      <div class="banner-botton">
        <ol class="carousel-indicators">
          <?php 
          $banners = CFS()->get('banners', $page_id);
          if ($banners):
            $i = 0;
            foreach ( $banners as $banner ):
          ?>
          <li data-target="#bannerHome" data-slide-to="<?php echo $i; ?>" class="<?php echo $i==0 ? 'active' : null; ?>"></li>
          <?
            $i++;
            endforeach;
          endif;
          ?>
        </ol>
      </div>
      <div class="carousel-inner">
        <?php 
        $banners = CFS()->get('banners', $page_id);
        if ($banners):
          $i = 0;
          foreach ( $banners as $banner ):
        ?>
        <div class="item <?php echo $i==0 ? 'active' : null; ?>" style="background-image: url('<?php echo $banner['imagem_banner']; ?>');">
          <div class="carousel-caption">
            <div class="carousel-text">
              <span class="line"></span>

              <?php if(!empty($banner['titulo_banner'])): ?>
              <h1><?php echo $banner['titulo_banner']; ?></h1>
              <?php endif; ?>

              <?php if(!empty($banner['subtitulo_banner'])): ?>
              <h2><?php echo $banner['subtitulo_banner']; ?></h2>
              <?php endif; ?>

            </div>
          </div>
        </div>
        <?
          $i++;
          endforeach;
        endif;
        ?>
      </div>
      <a class="left carousel-control" href="#bannerHome" data-slide="prev">
        <img src="<?php echo bloginfo('template_url'); ?>/img/down-arrow.svg" class="rotate90">
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#bannerHome" data-slide="next">
        <img src="<?php echo bloginfo('template_url'); ?>/img/down-arrow.svg" class="rotate-90">
        <span class="sr-only">Next</span>
      </a>
    </div>


        <?php
        $cat_args = array(
            'orderby'       => 'term_id', 
            'order'         => 'ASC',
            'hide_empty'    => true, 
        );
        $categories = get_terms('noticiacat', $cat_args);
        ?>
        <div class="filter">
            <select class="selectpicker" data-style="btn-danger" onchange="location.href=this.value">
                <option data-content="FILTRAR POR CATEGORIAS <i class='glyphicon glyphicon-filter'></i>"></option>
                <?php foreach($categories as $category): ?>
                <option value="<?php echo get_home_url(); ?>/categoria-noticias/<?php echo $category->slug ?>"><?php echo $category->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <?php if(!empty(CFS()->get('titulo_noticias', $page_id))): ?>
        <div class="top-title text-center">
            <h2><?php echo CFS()->get('titulo_noticias', $page_id); ?></h2>
        </div>
        <?php endif; ?>

        <div id="articles" class="container-fluid">
          <div class="scroll">

          <?php $i=0; while(have_posts()): the_post(); ?>
          <?php $terms = get_the_terms($post->ID, 'noticiacat' ); ?>

            <?php if( ($i % 6 == 0) || ($i % 6 == 1) ): ?>
              <?php if( ($i % 6 == 0) ): ?>
              <div class="row">
              <?php endif; ?>

                <?php if( ($i % 6 == 0) ): ?>
                <div class="col-sm-8">
                  <article>
                    <a href="<?php echo get_permalink(); ?>" class="article article-h clearfix">
                      <div class="article-image col-sm-6" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumb')[0]; ?>);"></div>
                      <div class="article-text col-sm-6">
                        <div class="cat" style="color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;">
                          <span class="line" style="background-color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;"></span>
                          <?php echo $terms[0]->name; ?>
                        </div>
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                      </div>
                    </a>
                  </article>
                </div>
                <?php endif; ?>

                <?php if( ($i % 6 == 1) ): ?>
                <div class="col-sm-4">
                  <article>
                    <a href="<?php echo get_permalink(); ?>" class="article article-h clearfix">
                      <div class="article-text">
                        <div class="cat" style="color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;">
                          <span class="line" style="background-color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;"></span>
                          <?php
                          echo $terms[0]->name;
                          ?>
                        </div>
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                      </div>
                    </a>
                  </article>
                </div>
                <?php endif; ?>

              <?php if( ($i % 6 == 1) ): ?>
              </div>
              <?php endif; ?>
            <?php endif; ?>


            <?php if( ($i % 6 == 2) || ($i % 6 == 3) || ($i % 6 == 4) || ($i % 6 == 5) ): ?>
              <?php if( ($i % 6 == 2) ): ?>
              <div class="row">
              <?php endif; ?>

                <?php if( ($i % 6 == 2) ): ?>
                <div class="col-sm-3">
                  <article>
                    <a href="<?php echo get_permalink(); ?>" class="article article-v">
                      <div class="article-image" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumb')[0]; ?>);"></div>
                      <div class="article-text">
                        <div class="cat" style="color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;">
                          <span class="line" style="background-color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;"></span>
                          <?php
                          echo $terms[0]->name;
                          ?>
                        </div>
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                      </div>
                    </a>
                  </article>
                </div>

                <div class="col-sm-9">
                <?php endif; ?>

                  <?php if( ($i % 6 == 3) ): ?>
                  <div class="row">
                    <div class="col-sm-12">
                      <article>
                        <a href="<?php echo get_permalink(); ?>" class="article article-h clearfix">
                          <div class="article-image col-sm-7" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumb')[0]; ?>);"></div>
                          <div class="article-text col-sm-5">
                            <div class="cat" style="color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;">
                              <span class="line" style="background-color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;"></span>
                              <?php
                              echo $terms[0]->name;
                              ?>
                            </div>
                            <h3><?php the_title(); ?></h3>
                            <p><?php the_excerpt(); ?></p>
                          </div>
                        </a>
                      </article>
                    </div>
                  </div>

                  <div class="row">
                  <?php endif; ?>

                  <?php if( ($i % 6 == 4) ): ?>
                    <div class="col-sm-4">
                      <article>
                        <a href="<?php echo get_permalink(); ?>" class="article article-v clearfix">
                          <div class="article-text">
                            <div class="cat" style="color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;">
                              <span class="line" style="background-color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;"></span>
                              <?php
                              echo $terms[0]->name;
                              ?>
                            </div>
                            <h3><?php the_title(); ?></h3>
                            <p><?php the_excerpt(); ?></p>
                          </div>
                        </a>
                      </article>
                    </div>
                  <?php endif; ?>

                  <?php if( ($i % 6 == 5) ): ?>
                    <div class="col-sm-8">
                      <article>
                        <a href="<?php echo get_permalink(); ?>" class="article article-h article-h-2 clearfix">
                          <div class="article-image col-sm-4" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumb')[0]; ?>);"></div>
                          <div class="article-text col-sm-8">
                            <div class="cat" style="color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;">
                              <span class="line" style="background-color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;"></span>
                              <?php
                              echo $terms[0]->name;
                              ?>
                            </div>
                            <h3><?php the_title(); ?></h3>
                            <p><?php the_excerpt(); ?></p>
                          </div>
                        </a>
                      </article>
                    </div>
                  <?php endif; ?>

                <?php if( ($i % 6 == 5) ): ?>
                  </div>
                </div>
                <?php endif; ?>

              <?php if( ($i % 6 == 5) ): ?>
              </div>
              <?php endif; ?>
            <?php endif; ?>

          <?php $i++; endwhile; wp_reset_query(); ?> 
          


              <?php if(!empty(get_next_posts_link())): ?>
              <div class="row" style="margin-bottom: 30px;">
                <div class="col-sm-12 text-center">
                  <a href="<?php echo get_next_posts_page_link() ?>" class="btn btn-danger btn-outline text-uppercase btn-plus next-page" style="width: 305px;">
                    Ver mais
                    <svg x="0px" y="0px" viewBox="0 0 400 400">
                    <g>
                      <g>
                        <path d="M199.995,0C89.716,0,0,89.72,0,200c0,110.279,89.716,200,199.995,200C310.277,400,400,310.279,400,200    C400,89.72,310.277,0,199.995,0z M199.995,373.77C104.182,373.77,26.23,295.816,26.23,200c0-95.817,77.951-173.77,173.765-173.77    c95.817,0,173.772,77.953,173.772,173.77C373.769,295.816,295.812,373.77,199.995,373.77z" fill="#D80027"/>
                        <path d="M279.478,186.884h-66.363V120.52c0-7.243-5.872-13.115-13.115-13.115s-13.115,5.873-13.115,13.115v66.368h-66.361    c-7.242,0-13.115,5.873-13.115,13.115c0,7.243,5.873,13.115,13.115,13.115h66.358v66.362c0,7.242,5.872,13.114,13.115,13.114    c7.242,0,13.115-5.872,13.115-13.114v-66.365h66.367c7.241,0,13.114-5.873,13.114-13.115    C292.593,192.757,286.72,186.884,279.478,186.884z" fill="#D80027"/>
                      </g>
                    </g>
                    </svg>
                  </a>
                </div>
              </div>
              <?php endif; ?>

          </div>

        </div>


    <?php get_template_part('includes/content','newsletter'); ?>

<?php get_footer(); ?>