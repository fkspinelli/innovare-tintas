<!DOCTYPE html>
<html>
<head>
  <title>Innovare</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Muli:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Ubuntu+Mono" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo bloginfo('template_url'); ?>/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_url'); ?>/css/style.css">
  <link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_url'); ?>/css/responsive.css">
  <script src="<?php echo bloginfo('template_url'); ?>/js/jquery.min.js"></script>
  <script src="<?php echo bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
  <script src="<?php echo bloginfo('template_url'); ?>/js/script.js"></script>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">

<header>
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#home">
          <img src="<?php echo bloginfo('template_url'); ?>/img/logo-innovare.png">
        </a>
      </div>
      <div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#" class="facebook" target="_blank"><img src="<?php echo bloginfo('template_url'); ?>/img/icon-facebook.png"></a></li>
            <li><a href="#" class="twitter" target="_blank"><img src="<?php echo bloginfo('template_url'); ?>/img/icon-twitter.png"></a></li>
            <li><a href="#" class="linkedin" target="_blank"><img src="<?php echo bloginfo('template_url'); ?>/img/icon-linkedin.png"></a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right links">
            <li><a href="#home">Home</a></li>
            <li><a href="#a-empresa">a empresa</a></li>
            <li><a href="#produtos">produtos</a></li>
            <li><a href="#contato">contato</a></li>
          </ul>
        </div>
      </div>
    </div>
  </nav>  
</header>  

<section>
  <div id="home">
    <div id="banner" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner">
        <div class="item active" style="background-image: url(<?php echo bloginfo('template_url'); ?>/img/banner-1.png);">
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-md-push-2">
                <div class="carousel-caption">
                  <div class="text">
                    <h1>Mais qualidade para sua impressão</h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item" style="background-image: url(<?php echo bloginfo('template_url'); ?>/img/banner-2.png);">
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-md-push-2">
                <div class="carousel-caption">
                  <div class="text">
                    <h1>Mais qualidade para sua impressão</h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item" style="background-image: url(<?php echo bloginfo('template_url'); ?>/img/banner-3.png);">
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-md-push-2">
                <div class="carousel-caption">
                  <div class="text">
                    <h1>Mais qualidade para sua impressão</h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="carousel-control-box">
          <a class="left carousel-control" href="#banner" data-slide="prev">
            <img src="<?php echo bloginfo('template_url'); ?>/img/seta-esq.png">
          </a>
          <a class="right carousel-control" href="#banner" data-slide="next">
            <img src="<?php echo bloginfo('template_url'); ?>/img/seta-dir.png">
          </a>
        </div>
      </div>
    </div> 
  </div> 
</section>

<section>
  <div id="a-empresa" class="empresa">
    <div class="container">
      <div class="row">
        <div class="col-sm-2 col-sm-push-1">
          <div class="bg" style="background-image: url(<?php echo bloginfo('template_url'); ?>/img/bg-empresa.png);"></div>
        </div>
        <div class="col-sm-10">
          <div class="border">
            <div class="row">
              <div class="col-sm-11 col-sm-push-1">
                <h2>A Empresa</h2>
                <div class="box">
                  A Innovare é uma empresa que está sempre perto de você! Excelência em seu trabalho e inovação em produtos e serviços são as marcas registradas desta empresa, que tem a consciência de que precisa estar em renovação constante, para atender da melhor forma seus clientes.
                </div>

                <div class="row">
                  <div class="col-sm-4">
                    <div class="icon"><img src="<?php echo bloginfo('template_url'); ?>/img/brasil.png"></div>
                    <h4>PRESENTE EM TODO PAÍS</h4>
                    <p>A Innovare é uma empresa que se orgulha de ser genuinamente brasileira, com capital 100% nacional, presente em todos os estados brasileiros.</p>
                  </div>
                  <div class="col-sm-4">
                    <div class="icon"><img src="<?php echo bloginfo('template_url'); ?>/img/pantone.png"></div>
                    <h4>LICENCIADA PANTONE®</h4>
                    <p>A Innovare é licenciada pela Pantone®. Procure uma de nossas revendas para obter outras informações sobre os Catálogos Pantone® e da Innovare.</p>
                  </div>
                  <div class="col-sm-4">
                    <div class="icon"><img src="<?php echo bloginfo('template_url'); ?>/img/cartucho.png"></div>
                    <h4>DIVERSOS TIPOS DE IMPRESSÃO </h4>
                    <p>Tintas gráficas adequadas a todos os tipos de impressão: offset, flexografia e metalgrafia.</p>
                  </div>
                </div>

                <hr>

                <h3>“Estar presente, na velocidade que nossos Clientes precisam, com tintas, produtos especiais e serviços de maior qualidade, tecnologia e valor agregado.”</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="produtos" class="produtos">
    <div class="container">
      <div class="row row-equal" data-max-width="768">
        <div class="col-md-3 col-height">
          <h2 class="bullet">Produtos</h2>
          <p>A Innovare disponibiliza uma gama completa de produtos de alta performance, tecnologicamente adequados aos diferentes tipos de equipamentos, e que proporcionam melhor desempenho das impressões, com consequente aumento de produtividade de nossos Clientes.</p>
          <h3 class="bullet">Fichas de Segurança e Boletim Técnico dos nossos produtos.</h3>
          <p>Os documentos abaixo disponibilizados foram elaborados para os produtos genéricos de cada segmento. Para obter o documento específico para o seu produto, entre em contato e solicite.</p>

          <div class="downloads">
            <a href="#" class="btn btn-warning btn-block text-uppercase clearfix">OffSet <img src="<?php echo bloginfo('template_url'); ?>/img/download-pdf.png"></a>
            <a href="#" class="btn btn-warning btn-block text-uppercase clearfix">Termosublimáveis <img src="<?php echo bloginfo('template_url'); ?>/img/download-pdf.png"></a>
            <a href="#" class="btn btn-warning btn-block text-uppercase clearfix">Liquidas Aquosas <img src="<?php echo bloginfo('template_url'); ?>/img/download-pdf.png"></a>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-height">
          <div class="box-produto">
            <div class="image" style="background-image: url(<?php echo bloginfo('template_url'); ?>/img/tintas-offset.png);"></div>
            <div class="text text-blue">
              <h3>Tintas <br> OffSet</h3>
              <p>A Innovare disponibiliza para o mercado gráfico de offset, tintas criteriosamente desenvolvidas para suprir quaisquer necessidades em diversos suportes, além de aplicações promocionais, embalagens, editoriais e comerciais.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-height">
          <div class="box-produto">
            <div class="image" style="background-image: url(<?php echo bloginfo('template_url'); ?>/img/tintas-especiais.jpg);"></div>
            <div class="text text-pink">
              <h3>Tintas <br> Especiais</h3>
              <p>Nos Laboratórios da Innovare são desenvolvidas tonalidades especiais para produtos, empresas, marcas e situações em que se requer uma padronização de cores impressas. As cores são desenvolvidas e aplicadas em condições ambientais controladas e com equipamentos de última geração.  Esses desenvolvimentos ocorrem em prazos extremamente curtos. <br> Comprove!</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-height">
          <div class="box-produto">
            <div class="image" style="background-image: url(<?php echo bloginfo('template_url'); ?>/img/tintas-especiais.jpg);"></div>
            <div class="text text-yellow">
              <h3>Tintas para <br> Flexografia</h3>
              <p>Possuímos uma variedade de produtos para aplicação nos mais diversos tipos de suporte celulósicos, para atender às inúmeras exigências quanto a resistências específicas.  Nos nossos Laboratórios também são desenvolvidas tonalidades especiais para quaisquer necessidades.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="contato" class="contato">
    <div class="onde-estamos">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 col-md-5">
            <h2><img src="<?php echo bloginfo('template_url'); ?>/img/pin.png"> Onde Estamos</h2>
          </div>
          <div class="col-lg-3 col-md-3">
            <p>
              <b>Filial RJ:</b> <br>
              Rua Conde de Agrolongo, 332 – Penha <br>
              CEP 21.020-190 – Rio de Janeiro – RJ <br>
              Telefones: (21) 3952-5201
            </p>
          </div>
          <div class="col-lg-3 col-md-3 col-lg-push-2">
            <p>
              <b>Filial SP:</b> <br>
              Rua Cecília Roizen, 904 – Cidade Industrial Satélite <br>
              CEP 02.222-020 - Guarulhos – SP <br>
              Telefones: (11) 3230-2702
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="mapa" style="background-image: url(<?php echo bloginfo('template_url'); ?>/img/mapa.png);">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <div class="form-contato">
              <h2>Entre em contato</h2>
              <?php echo do_shortcode('[contact-form-7 id="4" title="Contato"]'); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</body>
</html>
