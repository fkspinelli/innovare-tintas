<?php get_header(); the_post(); $page_noticias_id = 42; ?>
<?php $terms = get_the_terms($post->ID, 'noticiacat'); ?>

<?php if(!empty(get_the_title($page_noticias_id))): ?>
<div class="top-title-page">
  <div class="top-title text-center">
    <h1><?php echo get_the_title($page_noticias_id); ?></h1>
  </div>
</div>
<?php endif; ?>

<div class="banner" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'normal')[0]; ?>);">
  <img src="<?php echo bloginfo('template_url'); ?>/img/b-i-h.jpg">
</div>

<div id="articles" class="container-fluid noticia">

  <?php if(!empty($terms[0]->name)): ?>
  <div class="top-title text-center noticias">
    <h3><?php echo $terms[0]->name; ?></h3>
  </div>
  <?php endif; ?>

  <div class="row">
    <div class="col-sm-10 col-sm-push-1">
      <h1 class="text-center"><?php the_title(); ?></h1>
      
      <?php if(!empty(CFS()->get('subtitulo'))): ?>
      <h2 class="text-center"><?php echo CFS()->get('subtitulo'); ?></h2>
      <?php endif; ?>
      
      <div class="content">
        <?php the_content(); ?>
      </div>

      <?php if(!empty(CFS()->get('video'))): ?>
      <div>
        <?php echo CFS()->get('video'); ?>
      </div>
      <?php endif; ?>

    </div>
  </div>

</div>

<div class="container-fluid">
  <div class="share">
    <?php if(!empty(CFS()->get('titulo_compartilhe', $page_noticias_id))): ?>
    <div class="top-title text-center">
      <h2><?php echo CFS()->get('titulo_compartilhe', $page_noticias_id); ?></h2>
    </div>
    <?php endif; ?>
    <div class="share-jssocial"></div>
  </div>
</div>




<?php
query_posts( array(  
    'post_type' => 'noticias', 
    'posts_per_page' => 4, 
    'post__not_in' => array($post->ID),
    'tax_query' => array( 
        array( 
            'taxonomy' => 'noticiacat',
            'field' => 'id', 
            'terms' => $terms[0]->term_id
        ) 
    ) 
) );
if (have_posts()):
?>
<div class="container-fluid">
  <?php if(!empty(CFS()->get('titulo_posts_relacionados', $page_noticias_id))): ?>
  <div class="top-title text-center">
    <h2><?php echo CFS()->get('titulo_posts_relacionados', $page_noticias_id); ?></h2>
  </div>
  <?php endif; ?>

  <div class="row">
  <?php while(have_posts()): the_post(); $terms = get_the_terms($post->ID, 'noticiacat' );?>
    <div class="col-sm-3">
      <article>
        <a href="<?php echo get_permalink(); ?>" class="article article-h clearfix">
          <div class="article-image col-sm-12" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumb')[0]; ?>);"></div>
          <div class="article-text col-sm-12">
            <div class="cat" style="color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;">
              <span class="line" style="background-color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;"></span>
              <?php echo $terms[0]->name; ?>
            </div>
            <h3><?php the_title(); ?></h3>
            <p><?php the_excerpt(); ?></p>
          </div>
        </a>
      </article>
    </div>
  <?php endwhile; wp_reset_query(); ?>
  </div>
</div>
<?php endif; ?>



<?php
query_posts( array(  
    'post_type' => 'noticias', 
    'posts_per_page' => 4, 
    'post__not_in' => array($post->ID)
) );
if (have_posts()):
?>
<div class="container-fluid">
  <?php if(!empty(CFS()->get('titulo_ultimas_noticias', $page_noticias_id))): ?>
  <div class="top-title text-center">
    <h2><?php echo CFS()->get('titulo_ultimas_noticias', $page_noticias_id); ?></h2>
  </div>
  <?php endif; ?>
  <div class="row">
  <?php while(have_posts()): the_post(); $terms = get_the_terms($post->ID, 'noticiacat' );?>
    <div class="col-sm-3">
      <article>
        <a href="<?php echo get_permalink(); ?>" class="article article-h clearfix">
          <div class="article-text col-sm-12">
            <div class="cat" style="color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;">
              <span class="line" style="background-color: <?php echo get_term_meta($terms[0]->term_id, 'cc_color', true); ?>;"></span>
              <?php echo $terms[0]->name; ?>
            </div>
            <h3><?php the_title(); ?></h3>
            <p><?php the_excerpt(); ?></p>
          </div>
        </a>
      </article>
    </div>
  <?php endwhile; wp_reset_query(); ?>
  </div>
</div>

<div style="margin-bottom: 120px;"></div>
<?php endif; ?>


<?php get_template_part('includes/content','newsletter'); ?>
<?php get_footer(); ?>