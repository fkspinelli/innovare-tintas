<?php  
/*
* Template Name: Quero Ser Parceiro
*
*/
get_header();
the_post();
?>

    <div class="top-title-page">
      <div class="top-title text-center">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>

    <div class="banner banner-filter parceiro" style="background-image: url(<?php echo CFS()->get('banner'); ?>);">
      <div class="banner-caption">
        <div class="banner-text">
          <?php if (CFS()->get('titulo_do_banner')):  ?>
          <span class="line"></span>
          <h1><?php echo CFS()->get('titulo_do_banner'); ?></h1>
          <?php endif;  ?>

          <?php if (CFS()->get('subtitulo_do_banner')):  ?>
          <h2><?php echo CFS()->get('subtitulo_do_banner'); ?></h2>
          <?php endif;  ?>
        </div>
      </div>
      <img src="<?php echo bloginfo('template_url'); ?>/img/b-i-h.jpg">
    </div>


    <section>
          <?php if (CFS()->get('video')):  ?>
          <div class="video-parceiro">
            <?php echo CFS()->get('video'); ?>
          </div>
          <?php endif;  ?>

      <div class="container-fluid">

        <?php if (CFS()->get('texto_video')):  ?>
        <div class="top-title no-line text-center">
          <h2><?php echo CFS()->get('texto_video'); ?></h2>
        </div>
        <?php endif;  ?>

        <div class="box-form-contato parceiro">
          <div class="row">
            <div class="col-sm-6 col-sm-push-3">
              <?php echo do_shortcode(CFS()->get('formulario')); ?>
            </div>
          </div>
        </div>

        <div class="top-title text-center" style=" padding: 90px 0 0px; ">
          <h2>Conheça alguns de nossos parceiros</h2>
        </div>
        <div class="box-gray parceiro">
          <div class="center">
            <div class="row">

              <?php 
              $parceiros = CFS()->get('parceiros');
              if ($parceiros):
                foreach ( $parceiros as $parceiro ):
              ?>
              <div class="col-sm-2 col-xs-4">
                <img src="<?php echo $parceiro['logo']; ?>" class="img-responsive">
              </div>
              <?php
                endforeach;
              endif;
              ?>
            </div>
          </div>
      </div>

      </div>
    </section>

    <?php get_template_part('includes/content','newsletter'); ?>

<?php get_footer(); ?>